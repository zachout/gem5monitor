'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

const firestore = admin.firestore();

exports.fileChange = functions.firestore
    .document('/files/{fileId}')
    .onWrite((change, context) => {
	const file = change.after.data();
	const fileId = file.fileName;
	console.log(`File ${fileId} updated.`);
	const fileUrl = file.fileUrl;
	const timeStamp = file.timeStamp;
	const payload = {
   	    /*
	      data: {
   		fileName: String(fileId),
   		fileUrl: fileUrl,
		timeStamp: timeStamp
   		} */
	    notification: {
		title: `${fileId} updated.`,
		body: `Uploaded to ${fileUrl} at ${timeStamp}`
	    }
	};
	const options = {
   	    ttl: 60 * 1000,
   	    priority: 'high',
	};
	return pushMessage(payload, options);
    }, err => {
	console.log(`Encountered error: ${err}`);
	return false;
    });

function pushMessage(payload, options) {
    console.log('Sending notification: ', payload);
    admin.messaging().sendToTopic("updates", payload, options)
	.then((response) => {
	    console.log('Successfully sent message:', response);
	    return true;
	})
	.catch((err) => {
	    console.log('Error sending message:', err);
	    return false;
	});
}
