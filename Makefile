PROJECT := gem5monitor

SHELL := /bin/bash
MKDIR = mkdir -p
RMR := rm -rf

SRCDIR := src
PYDIR := gem5monitor

CWD := $(shell pwd)

PYC_FILES := $(shell find . -type f -name "*.pyc")
EDIT_FILES := $(shell find . -type f -name "*~")

GENERATED := \
	$(PYC_FILES) \
	$(EDIT_FILES) .eggs build dist gem5monitor.egg-info

PYLINT_FILES := $(shell find $(PYDIR) -name "*.py")

all: pylint

pylint: $(PYLINT_FILES)
	@echo "Running pylint on $(PYLINT_FILES)"
	pylint --rcfile=.pylintrc $(PYLINT_FILES) -f parseable -r n --ignore=$(PROTO_PY_TARGETS) && \
	pycodestyle $(PYLINT_FILES) --max-line-length=120 && \
	pydocstyle $(PYLINT_FILES)

clean:
	$(RMR) $(GENERATED)
