"""
    gem5monitor - Monitor files for changes and backup to dropbox.

"""
import os
import sys
import argparse
import signal
import logging

from .utils import constants
from .utils import firebase_utils
from .utils import inotify_utils

# logging.basicConfig(filename=LOGFILE, level=logging.DEBUG)
LOG = logging.getLogger(__name__)
LOG.propagate = False
FILEHANDLER = logging.FileHandler(constants.LOGFILE, 'w')
FILEHANDLER.setLevel(logging.INFO)
LOG.addHandler(FILEHANDLER)
KEEP_FDS = [FILEHANDLER.stream.fileno()]


def parse_args():
    """
    Parse arguments.

    """
    parser = argparse.ArgumentParser(
        prog='PROG', description='Watch files in gem5 m5out directory',
    )

    parser.add_argument('-c', '--certificatefile',
                        help='Firebase certificate file',
                        default=constants.CERTIFICATEFILE)

    parser.add_argument('-g', '--googlefile',
                        help='Google services file',
                        default=constants.GOOGLEJSONFILE)

    parser.add_argument('-b', '--benchdir',
                        help='Benchmark directory to watch',
                        default='/home/yannes/Dropbox/Gem5_Docs')

    parser.add_argument('-f', '--file',
                        help='File to watch',
                        # nargs = '+',
                        action='append')
    parser.add_argument('-e', '--emulate',
                        help='Do not actually start monitor',
                        action='store_true')

    group_print = parser.add_mutually_exclusive_group()
    group_print.add_argument('--start',
                             action='store_const',
                             help='Start gem5monitor daemon',
                             dest='command', const='start')
    group_print.add_argument('--stop',
                             action='store_const',
                             help='Stop gem5monitor daemon',
                             dest='command', const='stop')
    parser.set_defaults(command='start')

    args, _ = parser.parse_known_args()
    args = vars(args)
    LOG.info('Args: %s', args)

    return args


def main():
    """
    Run main loop.

    """
    args = parse_args()
    if args['emulate']:
        sys.exit(0)

    if args['command'] == 'start':
        cred = firebase_utils.login(args['certificatefile'], args['googlefile'])
        LOG.debug('Starting gem5monitor daemon')
        files = [os.path.abspath(f) for f in args['file']]
        watcher = inotify_utils.FileWatcher(args['benchdir'], files, cred, args['certificatefile'], args['googlefile'])
        # inotify_utils.watch_files(args['benchdir'], files)
    elif args['command'] == 'stop':
        with open(constants.PIDFILE, 'r') as rfh:
            pid = int(rfh.read())
        LOG.info('Stopping daemon with pid %d', pid)
        os.kill(pid, signal.SIGKILL)

    return 0
