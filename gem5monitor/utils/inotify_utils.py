"""
    Perform utility operations watching files using inotify.
"""
import os
import signal
import logging
import socket
import datetime
import mimetypes
from shutil import copyfile
import pyinotify
import functools
import asyncore

from gem5monitor.utils import constants
from gem5monitor.utils import firebase_utils

logging.basicConfig(filename=constants.LOGFILE, level=logging.INFO)
# logging.basicConfig(level=logging.INFO, format='%(message)s')
LOG = logging.getLogger(__name__)


def get_new_path(filename):
    """
    Get dropbox outpath for file.
    """
    tokens = filename.split('/')
    dname = tokens[-2]
    fname = '.'.join(tokens[-1].split('.')[:-1])
    suffix = tokens[-1].split('.')[-1]
    newfile = '{}.{}.{}'.format(socket.gethostname(), dname, fname)
    if suffix == 'terminal':
        newfile += '.terminal.txt'
    elif suffix == 'bmp':
        newfile += '.jpg'
    else:
        newfile += '.' + suffix
    return newfile

# PYNOTIFIER = pyinotify.AsyncNotifier(PYWM, MyEventHandler())

def copy_to_dropbox(filename):
    """
    Copy file to dropbox.

    """
    newpath = '{}/Dropbox/Gem5_Docs/{}'.format(os.path.expanduser('~'), get_new_path(filename))
    if newpath.endswith('jpg'):
        os.system('convert {} {}'.format(filename, newpath))
    else:
        copyfile(filename, newpath)
    # firebase_utils.upload_to_storage(newpath)
    return newpath


class FileWatcher(object):
    """
    Watch files and backup to firebase.

    """

    class MyEventHandler(pyinotify.ProcessEvent):
        """
        Handler for pyinotify events.

        """
        def __init__(self, certificatefile, googlefile, cred=None, readfreq=constants.READFREQ):
            self.cred = cred
            self.certificatefile = certificatefile
            self.googlefile = googlefile
            self.last_updates = {}
            self.readfreq = readfreq
            self.starttime = datetime.datetime.now()


        def process_IN_MODIFY(self, event):
            """
            When files are modified run copy_to_dropbox.

            """
            LOG.info('%s was modified', event.pathname)
            if (self.starttime - datetime.datetime.now()).seconds >= constants.REFRESHTIME:
                self.cred = firebase_utils.login(self.certificatefile, self.googlefile)

            if event.pathname in self.last_updates:
                if (datetime.datetime.now() - self.last_updates[event.pathname]).seconds < self.readfreq:
                    return
                # firebase_utils.login(self.certificatefile, self.googlefile)

            self.last_updates[event.pathname] = datetime.datetime.now()
            newpath = copy_to_dropbox(event.pathname)
            firebase_utils.upload_to_storage(newpath, self.certificatefile, self.googlefile)


    def __init__(self, benchdir, files, creds, certificatefile, googlefile, readfreq=constants.READFREQ):
        self.files = files
        self.readfreq = readfreq
        self.handler = FileWatcher.MyEventHandler(certificatefile, googlefile, creds, readfreq)
        self.pywm = pyinotify.WatchManager()
        self.notifier = pyinotify.AsyncNotifier(self.pywm, self.handler, read_freq=readfreq, timeout=constants.TIMEOUT)
        self.watch_files(benchdir, files, self.handler)


    def watch_files(self, benchdir, files, handler):
        """
        Watch files for changes.

        """
        LOG.info('Watching files: %s', files)
        mask = pyinotify.IN_ACCESS | pyinotify.IN_MODIFY
        if not files:
            self.pywm.add_watch(benchdir, mask, rec=True)
            LOG.info('Watching directory: %s', benchdir)
        else:
            for fname in files:
                self.pywm.add_watch(fname, mask, rec=False)
                LOG.info('Watching file: %s', fname)
        # PYNOTIFIER.loop()
        asyncore.loop()
