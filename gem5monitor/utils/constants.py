"""
    Constants.
"""
import os

LOGFILE = '{}/.gem5monitor.log'.format(os.path.expanduser('~'))
PIDFILE = '{}/.gem5monitor.pid'.format(os.path.expanduser('~'))
GOOGLEJSONFILE = '{}/Dropbox/Apps/python/gem5monitor/google-services.json'.format(os.path.expanduser('~'))
CERTIFICATEFILE = '{}/Dropbox/Apps/python/gem5monitor/gem5monitor-firebase.json'.format(os.path.expanduser('~'))

TIMEOUT = 1000
READFREQ = 30
REFRESHTIME = 1800
MAXLINES = 10
