"""
    Perform utility firebase operations.
"""
import json
import datetime
import time
import logging
import argparse
import mimetypes
import httplib2
import firebase_admin
from google.api_core import exceptions
from firebase_admin import credentials
from firebase_admin import firestore
from firebase_admin import storage
from oauth2client.service_account import ServiceAccountCredentials

from gem5monitor.utils import constants

logging.basicConfig(filename=constants.LOGFILE, level=logging.INFO)
# logging.basicConfig(level=logging.INFO, format='%(message)s')
LOG = logging.getLogger(__name__)


def parse_args():
    """
    Parse arguments.

    """
    parser = argparse.ArgumentParser(
        prog='PROG', description='Perform utility firebase operations',
    )

    parser.add_argument('-f', '--file',
                        help='File to watch',
                        default=constants.JSONFILE)

    args, _ = parser.parse_known_args()
    args = vars(args)

    return args


def get_project_url(googlefile):
    """
    Read firebase_url from google-services.json file.

    """
    with open(googlefile, 'r') as rfh:
        data = json.loads(rfh.read())

    return data['project_info']['firebase_url']


def get_storage_bucket(googlefile):
    """
    Read storage_bucket from google-services.json file.

    """
    with open(googlefile, 'r') as rfh:
        data = json.loads(rfh.read())

    return data['project_info']['storage_bucket']


def login(certificatefile, googlefile):
    """
    Login to firebase project using <project>-firebase.json file.

    """
    firebaseurl = get_project_url(googlefile)
    storagebucket = get_storage_bucket(googlefile)
    LOG.debug('Logging in to firebase %s', firebaseurl)
    cred = credentials.Certificate(certificatefile)
    try:
        firebase_admin.initialize_app(cred, options={
            'databaseURL': firebaseurl,
            'storageBucket': storagebucket,
        })
    except ValueError:
        LOG.debug('Already logged in')
    return cred


def refresh_token(cred):
    cred.refresh(httplib2.Http())
    return cred


def write_file_update(filename):
    """
    Write data to firestore 'files' collection under document 'filename'.

    """
    fileid = filename.split('/')[-1]
    client = firestore.client()
    doc_ref = client.collection('files').document(fileid)
    LOG.debug('Updating db: %s', fileid)
    return doc_ref.set({
        u'fileName': unicode(fileid),
        u'timeStamp': unicode(datetime.datetime.now().strftime('%y-%m-%d %H:%M:%S')),
    })


def upload_to_storage(filename, certificatefile, googlefile):
    """
    Write data to firestore 'files' collection under document 'filename' and
        save file data to storage.

    """
    fileid = filename.split('/')[-1]
    client = firestore.client()
    doc_ref = client.collection('files').document(fileid)

    try:
        with open(filename, 'rb') as rfh:
            file_data = rfh.read()
    except IOError:
        return None

    bucket = storage.bucket()
    blob = bucket.blob(fileid)
    filetype, _ = mimetypes.guess_type(filename)
    blob.upload_from_string(file_data, content_type=filetype)

    data = {
        u'fileName': unicode(fileid),
        u'timeStamp': unicode(datetime.datetime.now().strftime('%y-%m-%d %H:%M:%S')), # firestore.SERVER_TIMESTAMP,
        u'fileUrl': unicode(blob.public_url),
        u'mimeType': unicode(filetype),
    }
    LOG.info('Updating file %s', fileid)
    LOG.debug('Updating db: %s = %s', fileid, data)
    try:
        res = doc_ref.set(data)
    except exceptions.ServiceUnavailable:
        login(certificatefile, googlefile)
        res = doc_ref.set(data)
    return res
