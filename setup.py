"""
Setup for apk_launch project
"""
import distutils.cmd
import distutils.log
import subprocess
import os
import setuptools
from glob import glob

VERSION = '0.3'

from setuptools import setup, find_packages
from setuptools.extension import Extension
from setuptools.command.test import test as TestCommand

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setuptools.setup(
    name='gem5monitor',
    packages=setuptools.find_packages(exclude=('tests', 'docs')),
    license=open('LICENSE.txt').read(),
    include_package_data=True,
    version=VERSION,
    description='Monitor running gem5 instance',
    long_description=open('README.md').read(),
    author='Zach Yannes',
    author_email='zachyannes@gmail.com',
    url='https://github.com/Zachout',
    # download_url='https://github.com/Zachout/gem5monitor/archive/0.3.tar.gz',
    keywords=['gem5', 'monitor', 'backup'],
    classifiers=[],
    # setup_requires=['pytest-runner', 'pytest-pylint'],
    # tests_require=['pytest', 'pylint'],
    # cmdclass={
    #     'pylint': PylintCommand,
    # },
    install_requires = [
        requirements # 'pyinotify', 'httplib2', # 'google-cloud-firestore', 'firebase_admin'
    ],
    scripts=['scripts/gem5monitor'],
)
